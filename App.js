import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Linking,
  TextInput,
  NativeModules,
  ToastAndroid,
} from 'react-native';
import TaskServices from './Database/TaskServices';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const StackNavigator = createNativeStackNavigator();

const MainNavigation = () => {
  const linking = {
    prefixes: ['guinea://'],
  };
  return (
    <NavigationContainer linking={linking}>
      <StackNavigator.Navigator
        initialRouteName={'Home'}
        screenOptions={{
          headerShown: false,
        }}>
        <StackNavigator.Screen name={'Home'} component={App} />
      </StackNavigator.Navigator>
    </NavigationContainer>
  );
};

const index = () => {
  return <MainNavigation />;
};

const App = ({route}) => {
  const [link, setLink] = useState('');
  const [feedback, setFeedback] = useState({});

  useEffect(() => {
    if (route.params) {
      setFeedback(route.params);
    }
  }, [route.params]);

  const openUrl = async () => {
    const isCanOpen = await Linking.canOpenURL(link);
    if (isCanOpen) {
      await Linking.openURL(link);
    } else {
      ToastAndroid.show('Link Not Valid', ToastAndroid.LONG);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'Insert Deeplink'}
        value={link}
        onChangeText={val => setLink(val)}
      />

      <TouchableOpacity
        activeOpacity={0.8}
        onPress={openUrl}
        style={styles.button}>
        <Text style={styles.text}>Run DeepLink</Text>
      </TouchableOpacity>

      <Text>Feedback Deeplink</Text>
      <Text>{JSON.stringify(feedback)}</Text>
    </View>
  );
};

export default index;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'blue',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 20,
    marginVertical: 20,
  },
  text: {
    color: '#FFF',
  },
});
