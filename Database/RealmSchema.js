import Realm from 'realm';
import ModelTables from './ModelTables';

// Initialize a Realm with models
// var defaultPath = Realm.defaultPath;
// var newPath     = defaultPath.substring(0, defaultPath.lastIndexOf('/')) + '/default.realm';
let realmSchema = new Realm({
  // path: newPath,
  schema: [ModelTables.TR_LOG],
  schemaVersion: ModelTables.SCHEMA_VERSION,
});

export default realmSchema;
