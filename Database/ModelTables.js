/* eslint-disable prettier/prettier */
const SCHEMA_VERSION = 35;

// TR = Transaksi
// TM = Master

const TR_LOG = {
    name: 'TR_LOG',
    primaryKey: 'ID',
    properties: {
        ID: 'int?',
        LOG: 'string?',
    },
};

export default {
    TR_LOG,
    SCHEMA_VERSION,
};


