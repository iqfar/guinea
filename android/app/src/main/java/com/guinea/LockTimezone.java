package com.guinea;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.security.Provider;

public class LockTimezone extends ReactContextBaseJavaModule {

    public LockTimezone(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "LockTimezone";
    }

    Context context = getReactApplicationContext();

    @ReactMethod
    public void lock (Promise promise) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Settings.Global.putInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 1);
            } else {
                Settings.System.putInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 1);
            }
            promise.resolve("Timezone Locked");
        } catch (Exception e) {
            promise.reject(e);
        }
    }

}
