package com.guinea;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.app.Activity;
import android.content.Intent;
import com.facebook.react.bridge.Callback;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.react.ReactActivity;
import com.nexmo.client.NexmoCall;
import com.nexmo.client.NexmoCallEventListener;
import com.nexmo.client.NexmoCallHandler;
import com.nexmo.client.NexmoCallMemberStatus;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoMediaActionState;
import com.nexmo.client.NexmoMember;
import com.nexmo.client.request_listener.NexmoApiError;
import com.nexmo.client.request_listener.NexmoConnectionListener;
import com.nexmo.client.request_listener.NexmoRequestListener;

import android.content.Context;
import android.telecom.Call;
import android.util.Log;

public class CallModule extends ReactContextBaseJavaModule
{

    public CallModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }


    @Override
    public String getName() {
        return "CallModule";
    }

    Context context = getReactApplicationContext();

    private NexmoClient client = new NexmoClient.Builder().build(context);
    private NexmoCall onGoingCall;


    @ReactMethod
    public void setConnection(Callback callback) {
        client.setConnectionListener((connectionStatus, connectionStatusReason) -> {
            if (connectionStatus == NexmoConnectionListener.ConnectionStatus.CONNECTED) {
                client.addIncomingCallListener(it -> {
                    onGoingCall = it;
                    callback.invoke(connectionStatus, it);
                });
            } else {
                callback.invoke(connectionStatus, connectionStatusReason);
            }
        });
    }

    @SuppressLint("MissingPermission")
    @ReactMethod
    public void startCall(String token, String userToCall, Callback callback) {
        client.login("token");
        client.call(userToCall, NexmoCallHandler.SERVER, new NexmoRequestListener<NexmoCall>() {
            @Override
            public void onError(@NonNull NexmoApiError error) {
                callback.invoke(error);
            }

            @Override
            public void onSuccess(@Nullable NexmoCall result) {
                onGoingCall = result;
                onGoingCall.addCallEventListener(new NexmoCallEventListener() {
                    @Override
                    public void onMemberStatusUpdated(NexmoCallMemberStatus status, NexmoMember member) {
                        if (status == NexmoCallMemberStatus.CANCELLED || status == NexmoCallMemberStatus.COMPLETED) {
                            onGoingCall = null;
                        }
                        callback.invoke(status);
                    }

                    @Override
                    public void onMuteChanged(NexmoMediaActionState newState, NexmoMember member) {

                    }

                    @Override
                    public void onEarmuffChanged(NexmoMediaActionState newState, NexmoMember member) {

                    }

                    @Override
                    public void onDTMF(String dtmf, NexmoMember member) {

                    }
                });
            }
        });
    }

    @SuppressLint("MissingPermission")
    @ReactMethod
    public void answerCall(Callback callback) {
        onGoingCall.answer(new NexmoRequestListener<NexmoCall>() {
            @Override
            public void onError(@NonNull NexmoApiError error) {
                callback.invoke(error);
            }
            @Override
            public void onSuccess(@Nullable NexmoCall result) {
                callback.invoke(result);
            }
        });
    }

    @ReactMethod
    public void rejectCall(Callback callback) {
        onGoingCall.hangup(new NexmoRequestListener<NexmoCall>() {
            @Override
            public void onError(@NonNull NexmoApiError error) {
                callback.invoke(error);
            }

            @Override
            public void onSuccess(@Nullable NexmoCall result) {
                callback.invoke(result);
            }
        });
        onGoingCall = null;
    }

    @ReactMethod
    public void endCall(Callback callback) {
        onGoingCall.hangup(new NexmoRequestListener<NexmoCall>() {
            @Override
            public void onError(@NonNull NexmoApiError error) {
                callback.invoke(error);
            }

            @Override
            public void onSuccess(@Nullable NexmoCall result) {
                callback.invoke(result);
            }
        });
        onGoingCall = null;
    }



}